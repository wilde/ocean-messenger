import React, { PropTypes } from 'react';

function Thread(props) {
  const thread = props.thread;
  return (
    <div className="thread" key={thread.id}>
      <div>
        <span className="name">{thread.name}</span>
        <span className="unreadCount">{thread.unreadCount}</span>
      </div>
      <span className="latest">{thread.latest}</span>
    </div>
  );
}

Thread.propTypes = {
  thread: PropTypes.object.isRequired,
};

export default Thread;
