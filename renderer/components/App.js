import '../styles/App.scss';
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { fetchThreads } from '../actions';
import ThreadList from './ThreadList';

class App extends Component {
  componentDidMount() {
    fetchThreads();
  }
  render() {
    return <ThreadList threads={this.props.threads} />;
  }
}

App.propTypes = {
  dispatch: PropTypes.func.isRequired,
  threads: PropTypes.array.isRequired,
};

export default connect(state => state)(App);
