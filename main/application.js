import { BrowserWindow } from 'electron';
import fs from 'fs';
import promisify from 'es6-promisify';
import Slack from './slack';

const IN_MEMORY = ':memory:';
export default class Application {
  constructor(inMemory) {
    this.inMemory = inMemory;
  }
  loadAppURL() {
    // const dist = `file://${__dirname}/index.html`;
    this.mainWindow.loadURL('http://localhost:8080');
  }
  async slackDBPath() {
    let slackDBPath;
    if (this.inMemory) {
      slackDBPath = IN_MEMORY;
    } else {
      try {
        await promisify(fs.mkdir, fs)('db');
      } catch (e) {
        // No-op
      }
      slackDBPath = 'db/slack.db';
    }
    return slackDBPath;
  }
  async onAppReady() {
    this.mainWindow = new BrowserWindow({ width: 800, height: 600 });

    this.slackStore = new Slack(await this.slackDBPath());
    await this.slackStore.upgradeSchema();
    await this.slackStore.authenticate(this.mainWindow);
    await this.slackStore.listenForEvents();

    this.loadAppURL();
    this.mainWindow.webContents.openDevTools();

    this.mainWindow.on('closed', async () => {
      await this.slackStore.close();
      this.slackStore = null;

      this.mainWindow = null;
    });
  }
}
