import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import createLogger from 'redux-logger';
import { Provider } from 'react-redux';
import rootReducer from './reducers';
import App from './components/App';
import { ipcRenderer } from 'electron';

const store = createStore(rootReducer, undefined,
  applyMiddleware(createLogger()));

ipcRenderer.on('slack', (event, action) => {
  store.dispatch(action);
});

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('app')
);
