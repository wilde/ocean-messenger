import promisify from 'es6-promisify';
import nock from 'nock';
import proxyquire from 'proxyquire';
import EventEmitter from 'events';
import install from 'jasmine-es6/overrides/async';
install();

let electron;
let Slack;
describe('Slack backend', () => {
  beforeEach(() => {
    const mockIpcMain = new EventEmitter();
    electron = { ipcMain: mockIpcMain, '@noCallThru': true };
    Slack = proxyquire('../../main/slack', { electron }).default;
  });
  it('should open a DB', () => {
    const slackStore = new Slack(':memory:');
    expect(slackStore.db).toBeDefined();
  });
  describe('#upgradeSchema', () => {
    it('should create the schema', async () => {
      const slackStore = new Slack(':memory:');
      await slackStore.upgradeSchema();

      const all = promisify(slackStore.db.all, slackStore.db);
      const rows = await all('SELECT name FROM sqlite_master');

      expect(rows).toContain({ name: 'config' });
      expect(rows).toContain({ name: 'users' });
      expect(rows).toContain({ name: 'threads' });
      expect(rows).toContain({ name: 'thread_users' });
      expect(rows).toContain({ name: 'messages' });
    });
    it('should be idempotent', async () => {
      const slackStore = new Slack(':memory:');
      await slackStore.upgradeSchema();
      await slackStore.upgradeSchema();
    });
    it('should fetch the token from the config, if it is present', async () => {
      const slackStore = new Slack(':memory:');
      await slackStore.upgradeSchema();

      const run = promisify(slackStore.db.run, slackStore.db);
      await run("INSERT INTO config (id, token) VALUES (0, 'token')");
      await slackStore.upgradeSchema();
      expect(slackStore.token).toEqual('token');
    });
  });
  describe('#upsertRow', () => {
    it('should insert a new row if the id is missing', async () => {
      const slackStore = new Slack(':memory:');
      await slackStore.upgradeSchema();
      const user = {
        id: 'U123', name: 'face', color: 'ffefef', real_name: 'Face Face',
      };

      await slackStore.upsertRow('users', ['id'], user);

      const all = promisify(slackStore.db.all, slackStore.db);
      const users = await all('SELECT * FROM users');
      expect(users.length).toEqual(1);
      expect(users[0]).toEqual(jasmine.objectContaining(user));
    });

    it('should update an existing row if it exists', async () => {
      const slackStore = new Slack(':memory:');
      await slackStore.upgradeSchema();
      await slackStore.upsertRow('users', ['id'], {
        id: 'U123', name: 'face', color: 'ffefef', real_name: 'Face Face',
      });

      const updatedRow = {
        id: 'U123', name: 'face2', color: 'ffefef2', real_name: 'Face Face2',
      };
      await slackStore.upsertRow('users', ['id'], updatedRow);

      const all = promisify(slackStore.db.all, slackStore.db);
      const users = await all('SELECT * FROM users');
      expect(users.length).toEqual(1);
      expect(users[0]).toEqual(jasmine.objectContaining(updatedRow));
    });
  });
  describe('#upsertUsers', () => {
    describe('when the request is successful', () => {
      it('should insert all users into the DB', async () => {
        const users = [
          {
            id: 'U0U1G19KL',
            team_id: 'T0U0W15MJ',
            name: 'wilde',
            deleted: false,
            status: null,
            color: '9f69e7',
            real_name: 'Matt Wilde',
            tz: 'America\/Los_Angeles',
            tz_label: 'Pacific Daylight Time',
            tz_offset: -25200,
            profile: {
              image_512: 'https:\/\/secure.gravatar.com\/avatar',
            },
          },
          {
            id: 'USLACKBOT',
            team_id: 'T0U0W15MJ',
            name: 'slackbot',
            deleted: false,
            status: null,
            color: '757575',
            real_name: 'slackbot',
            tz: 'America\/Los_Angeles',
            tz_label: 'Pacific Daylight Time',
            tz_offset: -25200,
            profile: {
              image_512: 'https:\/\/secure.gravatar.com\/avatar',
            },
          },
        ];

        const slackStore = new Slack(':memory:');
        await slackStore.upgradeSchema();
        slackStore.token = 'token';
        await slackStore.upsertUsers(users);

        const all = promisify(slackStore.db.all, slackStore.db);
        const insertedUsers = await all('SELECT * FROM users');
        expect(insertedUsers.length).toEqual(2);
        expect(insertedUsers.map((user) => user.id)).toEqual(jasmine.arrayContaining([
          'U0U1G19KL', 'USLACKBOT',
        ]));
      });
    });
  });
  describe('#upsertChannels', () => {
    it('should insert all channels into the DB', async () => {
      const channels = [
        {
          id: 'C0U1G1AHY',
          name: 'general',
          is_channel: true,
          created: 1458439621,
          creator: 'U0U1G19KL',
          is_archived: false,
          is_general: true,
          is_member: true,
          members: ['U0U1G19KL'],
          num_members: 1,
        },
        {
          id: 'C0U192600',
          name: 'random',
          is_channel: true,
          created: 1458439621,
          creator: 'U0U1G19KL',
          is_archived: false,
          is_general: true,
          is_member: true,
          members: ['U0U1G19KL'],
          num_members: 1,
        },
      ];

      const slackStore = new Slack(':memory:');
      await slackStore.upgradeSchema();
      await slackStore.upsertRow('users', ['id'], {
        id: 'U0U1G19KL',
        name: 'wilde',
        color: 'fffefe',
        real_name: 'Matt Wilde',
      });
      slackStore.token = 'token';
      await slackStore.upsertChannels(channels);

      const all = promisify(slackStore.db.all, slackStore.db);
      const threads = await all('SELECT * FROM threads');
      expect(threads.length).toEqual(2);
      expect(threads.map((thread) => thread.id)).toEqual(jasmine.arrayContaining([
        'C0U1G1AHY', 'C0U192600',
      ]));

      const threadUsers = await all('SELECT * FROM thread_users');
      expect(threadUsers.length).toEqual(2);

      await slackStore.upsertChannels(channels);
      const threadUsers2 = await all('SELECT * FROM thread_users');
      expect(threadUsers2.length).toEqual(2);
    });
  });
  describe('#syncMessages', () => {
    describe('when the request is unsuccessful', () => {
      it('should throw', async () => {
        nock('https://slack.com/api')
          .get('/channels.history')
          .query({ token: 'token', channel: 'channel', latest: 0 })
          .reply(200, {
            ok: false,
            error: 'invalid_auth',
          });

        const slackStore = new Slack(':memory:');
        await slackStore.upgradeSchema();
        slackStore.token = 'token';
        let caughtError;
        try {
          await slackStore.syncMessages('channel');
        } catch (error) {
          caughtError = error;
        }
        expect(caughtError).toEqual('invalid_auth');
      });
    });
    describe('when the request is successful', () => {
      it('should insert all messages into the DB', async () => {
        nock('https://slack.com/api')
          .get('/channels.history')
          .query({ token: 'token', channel: 'channel', latest: 0 })
          .reply(200, {
            ok: true,
            messages: [
              {
                type: 'message',
                user: 'U0U1G19KL',
                text: 'Whattup',
                ts: '1458440004.000002',
              },
              {
                user: 'U0U1G19KL',
                type: 'message',
                subtype: 'channel_join',
                text: '<@U0U1G19KL|wilde> has joined the channel',
                ts: '1458439621.398179',
              },
            ],
          });

        const slackStore = new Slack(':memory:');
        await slackStore.upgradeSchema();
        slackStore.token = 'token';
        await slackStore.upsertRow('threads', ['id'], {
          id: 'channel',
          name: 'general',
          created: 1458439621,
          creator: 'U0U1G19KL',
        });
        await slackStore.syncMessages('channel');

        const all = promisify(slackStore.db.all, slackStore.db);
        const messages = await all('SELECT * FROM messages ORDER BY timestamp ASC');
        expect(messages.length).toEqual(2);
        expect(messages[0]).toEqual(jasmine.objectContaining({
          user_id: 'U0U1G19KL',
          type: 'channel_join',
          text: '<@U0U1G19KL|wilde> has joined the channel',
          timestamp: 1458439621.398179,
        }));
        expect(messages[1]).toEqual(jasmine.objectContaining({
          user_id: 'U0U1G19KL',
          text: 'Whattup',
          timestamp: 1458440004.000002,
        }));
      });
    });
  });
  describe('#listenForEvents', () => {
    describe('when the request is unsuccessful', () => {
      it('should throw', async () => {
        nock('https://slack.com/api')
          .get('/rtm.start')
          .query({ token: 'token', mpim_aware: true, simple_latest: true })
          .reply(200, {
            ok: false,
            error: 'invalid_auth',
          });

        const slackStore = new Slack(':memory:');
        await slackStore.upgradeSchema();
        slackStore.token = 'token';
        let caughtError;
        try {
          await slackStore.listenForEvents();
        } catch (error) {
          caughtError = error;
        }
        expect(caughtError).toEqual('invalid_auth');
      });
    });
    describe('when the request is unsuccessful due to web socket error', () => {
      const oldSlack = Slack;
      beforeEach(() => {
        class MockWebSocket extends EventEmitter {
          connect() {
            this.emit('connectFailed', 'SOMETHING TERRIBLE');
          }
        }
        Slack = proxyquire('../../main/slack', {
          electron,
          websocket: {
            client: MockWebSocket,
          },
        }).default;
      });
      afterEach(() => { Slack = oldSlack; });
      it('should throw', async () => {
        nock('https://slack.com/api')
          .get('/rtm.start')
          .query({ token: 'token', mpim_aware: true, simple_latest: true })
          .reply(200, {
            ok: true,
            url: 'wss://example.com',
          });

        const slackStore = new Slack(':memory:');
        await slackStore.upgradeSchema();
        slackStore.token = 'token';
        let caughtError;
        try {
          await slackStore.listenForEvents();
        } catch (error) {
          caughtError = error;
        }
        expect(caughtError).toEqual('SOMETHING TERRIBLE');
      });
    });
    describe('when the request is successful', () => {
      const oldSlack = Slack;
      let mockConnection;
      beforeEach(() => {
        class MockWebSocket extends EventEmitter {
          connect() {
            mockConnection = new EventEmitter();
            this.emit('connect', mockConnection);
          }
        }
        Slack = proxyquire('../../main/slack', {
          electron,
          websocket: {
            client: MockWebSocket,
          },
        }).default;
      });
      afterEach(() => { Slack = oldSlack; });
      it('should insert all users into the DB', async () => {
        nock('https://slack.com/api')
          .get('/rtm.start')
          .query({ token: 'token', mpim_aware: true, simple_latest: true })
          .reply(200, {
            ok: true,
            users: [
              {
                id: 'U0U1G19KL',
                team_id: 'T0U0W15MJ',
                name: 'wilde',
                deleted: false,
                status: null,
                color: '9f69e7',
                real_name: 'Matt Wilde',
                tz: 'America\/Los_Angeles',
                tz_label: 'Pacific Daylight Time',
                tz_offset: -25200,
                profile: {
                  image_512: 'https:\/\/secure.gravatar.com\/avatar',
                },
              },
              {
                id: 'USLACKBOT',
                team_id: 'T0U0W15MJ',
                name: 'slackbot',
                deleted: false,
                status: null,
                color: '757575',
                real_name: 'slackbot',
                tz: 'America\/Los_Angeles',
                tz_label: 'Pacific Daylight Time',
                tz_offset: -25200,
                profile: {
                  image_512: 'https:\/\/secure.gravatar.com\/avatar',
                },
              },
            ],
            channels: [
              {
                id: 'C0U1G1AHY',
                name: 'general',
                is_channel: true,
                created: 1458439621,
                creator: 'U0U1G19KL',
                is_archived: false,
                is_general: true,
                is_member: true,
                members: ['U0U1G19KL'],
                num_members: 1,
              },
              {
                id: 'C0U192600',
                name: 'random',
                is_channel: true,
                created: 1458439621,
                creator: 'U0U1G19KL',
                is_archived: false,
                is_general: true,
                is_member: true,
                members: ['U0U1G19KL'],
                num_members: 1,
              },
            ],
            url: 'wss://mpmulti-dg4c.slack-msgs.com/websocket/',
          });

        const slackStore = new Slack(':memory:');
        await slackStore.upgradeSchema();
        slackStore.token = 'token';
        await slackStore.listenForEvents();

        const all = promisify(slackStore.db.all, slackStore.db);
        const insertedUsers = await all('SELECT * FROM users');
        expect(insertedUsers.length).toEqual(2);

        const threads = await all('SELECT * FROM threads');
        expect(threads.length).toEqual(2);
        expect(threads.map((thread) => thread.id)).toEqual(jasmine.arrayContaining([
          'C0U1G1AHY', 'C0U192600',
        ]));

        const threadUsers = await all('SELECT * FROM thread_users');
        expect(threadUsers.length).toEqual(2);

        expect(mockConnection.listeners('message').length).toEqual(1);
      });
    });
  });
  describe('#onWebsocketMessage', () => {
    it('should reject binary messages', () => {
      const slackStore = new Slack(':memory:');
      expect(() => slackStore.onWebsocketMessage({ type: 'binary' })).toThrow();
    });
    it('should store messages', async () => {
      const slackStore = new Slack(':memory:');
      await slackStore.upgradeSchema();
      await slackStore.upsertRow('threads', ['id'], {
        id: 'C0U1G1AHY',
        name: 'general',
        created: 1458439621,
        creator: 'U0U1G19KL',
      });
      await slackStore.onWebsocketMessage({
        type: 'utf8',
        utf8Data: JSON.stringify({
          type: 'message',
          channel: 'C0U1G1AHY',
          user: 'U0U1G19KL',
          text: 'Whattup',
          ts: '1458440004.000002',
        }),
      });
      const all = promisify(slackStore.db.all, slackStore.db);
      const messages = await all('SELECT * FROM messages');
      expect(messages.length).toEqual(1);
    });
    it('should store channels', async () => {
      const slackStore = new Slack(':memory:');
      await slackStore.upgradeSchema();
      await slackStore.onWebsocketMessage({
        type: 'utf8',
        utf8Data: JSON.stringify({
          type: 'channel_created',
          channel: {
            id: 'C0U1G1AHY',
            name: 'general',
            is_channel: true,
            created: 1458439621,
            creator: 'U0U1G19KL',
            is_archived: false,
            is_general: true,
            is_member: true,
            members: ['U0U1G19KL'],
            num_members: 1,
          },
        }),
      });
      const all = promisify(slackStore.db.all, slackStore.db);
      const threads = await all('SELECT * FROM threads');
      expect(threads.length).toEqual(1);
    });
    it('should store users', async () => {
      const slackStore = new Slack(':memory:');
      await slackStore.upgradeSchema();
      for (const eventName of ['team_join', 'user_change']) {
        await slackStore.onWebsocketMessage({
          type: 'utf8',
          utf8Data: JSON.stringify({
            type: eventName,
            user: {
              id: 'U0U1G19KL',
              team_id: 'T0U0W15MJ',
              name: 'wilde',
              deleted: false,
              status: null,
              color: '9f69e7',
              real_name: 'Matt Wilde',
              tz: 'America\/Los_Angeles',
              tz_label: 'Pacific Daylight Time',
              tz_offset: -25200,
              profile: {
                image_512: 'https:\/\/secure.gravatar.com\/avatar',
              },
            },
          }),
        });
      }
      const all = promisify(slackStore.db.all, slackStore.db);
      const users = await all('SELECT * FROM users');
      expect(users.length).toEqual(1);
    });
  });
  describe('#getToken', () => {
    describe('when the request is unsuccessful', () => {
      it('should throw', async () => {
        nock('https://slack.com/api')
          .get('/oauth.access')
          .query({
            client_id: Slack.clientId,
            client_secret: Slack.clientSecret,
            code: 'code',
          })
          .reply(200, {
            ok: false,
            error: 'invalid_auth',
          });

        const slackStore = new Slack(':memory:');
        await slackStore.upgradeSchema();
        let caughtError;
        try {
          await slackStore.getToken('code');
        } catch (error) {
          caughtError = error;
        }
        expect(caughtError).toEqual('invalid_auth');
      });
    });
    describe('when the request is successful', () => {
      it('should set the token and save it to the config', async () => {
        nock('https://slack.com/api')
          .get('/oauth.access')
          .query({
            client_id: Slack.clientId,
            client_secret: Slack.clientSecret,
            code: 'code',
          })
          .reply(200, {
            ok: true,
            access_token: 'token',
            scope: 'read,client,identify,post',
            user_id: 'U0U1G19KL',
            team_name: 'Ocean Chat',
            team_id: 'T0U0W15MJ',
          });
        const slackStore = new Slack(':memory:');
        await slackStore.upgradeSchema();
        await slackStore.getToken('code');
        expect(slackStore.token).toEqual('token');
        const get = promisify(slackStore.db.get, slackStore.db);
        const config = await get('SELECT * FROM config WHERE id = 0');
        expect(config.token).toEqual('token');
      });
    });
  });
  describe('#authenticate', () => {
    it('should return immediately when the token is set', async () => {
      const slackStore = new Slack(':memory:');
      await slackStore.upgradeSchema();
      slackStore.token = 'token';
      await slackStore.authenticate();
    });
    it('should navigate the window when the token is not set', async () => {
      const mockWindow = {
        loadURL: jasmine.createSpy('loadURL'),
        webContents: new EventEmitter(),
      };
      const slackStore = new Slack(':memory:');
      spyOn(slackStore, 'getToken').and.returnValue(Promise.resolve('token'));
      await slackStore.upgradeSchema();
      const authPromise = slackStore.authenticate(mockWindow);
      expect(mockWindow.loadURL).toHaveBeenCalled();
      mockWindow.webContents.emit('will-navigate', null, 'http://example.com?code=code');
      await authPromise;
    });
  });
  describe('#onRendererRequest', () => {
    it('should call onFetchThreads when it receives a FETCH_THREADS message', () => {
      const slackStore = new Slack(':memory:');
      spyOn(slackStore, 'onFetchThreads');
      slackStore.onRendererRequest({ sender: 'sender' }, { type: 'FETCH_THREADS' });
      expect(slackStore.onFetchThreads).toHaveBeenCalledWith('sender');
    });
  });
  describe('#onFetchThreads', () => {
    it('should send a thread list back to the sender', async () => {
      const slackStore = new Slack(':memory:');
      await slackStore.upgradeSchema();
      slackStore.token = 'token';
      const thread = {
        id: 'channel',
        name: 'general',
        created: 1458439621,
        creator: 'U0U1G19KL',
      };
      await slackStore.upsertRow('threads', ['id'], thread);
      const sender = { send: jasmine.createSpy() };
      await slackStore.onFetchThreads(sender);
      expect(sender.send).toHaveBeenCalledWith('slack',
        { type: 'RECEIVE_THREADS', threads: [thread] });
    });
  });
});
