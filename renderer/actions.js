import { ipcRenderer } from 'electron';

export function fetchThreads() {
  ipcRenderer.send('slack', { type: 'FETCH_THREADS' });
}
