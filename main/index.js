import 'babel-polyfill';
import { app } from 'electron';
import Application from './application';

const mainApp = new Application();
app.on('ready', mainApp.onAppReady.bind(mainApp));
