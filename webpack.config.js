const nodeExternals = require('webpack-node-externals');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = [
  {
    target: 'electron',
    node: {
      __dirname: false,
      __filename: false,
    },
    entry: './main/index.js',
    output: { path: 'dist', filename: 'main.js' },
    externals: [nodeExternals()],
    module: {
      loaders: [
        {
          test: /.js$/,
          loader: 'babel-loader',
          exclude: /node_modules/,
          query: {
            presets: ['es2015'],
            plugins: ['transform-async-to-generator'],
          },
        },
      ],
    },
  },
  {
    entry: './renderer/index.js',
    output: { path: 'dist', filename: 'renderer.js' },
    externals: { electron: 'commonjs electron' },
    module: {
      loaders: [
        {
          test: /.jsx?$/,
          loaders: ['react-hot', 'babel?presets[]=es2015,presets[]=react'],
          exclude: /node_modules/,
        },
        {
          test: /\.scss$/,
          loaders: ['style', 'css', 'sass'],
          exclude: /node_modules/,
        },
      ],
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: './renderer/index.html',
      }),
    ],
  },
];
