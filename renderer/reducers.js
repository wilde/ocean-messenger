import { combineReducers } from 'redux';

function threads(state = [], action) {
  switch (action.type) {
    case 'RECEIVE_THREADS':
      return action.threads;
    default:
      return state;
  }
}

const rootReducer = combineReducers({
  threads,
});

export default rootReducer;
