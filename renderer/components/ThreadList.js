import React, { PropTypes } from 'react';
import Thread from './Thread';

function ThreadList(props) {
  return (
    <div className="threadList">
      {props.threads.map((thread) =>
        <Thread key={thread.id} thread={thread} />
      )}
    </div>
  );
}

ThreadList.propTypes = {
  threads: PropTypes.array.isRequired,
};

export default ThreadList;
