import rootReducer from '../../renderer/reducers';

describe('Reducer', () => {
  it('returns an initial state', () => {
    const state = rootReducer(undefined, {});
    expect(state).toEqual({
      threads: [],
    });
  });
  it('receives threads', () => {
    const threads = [{ id: 'iAmAThread' }];
    const state = rootReducer(undefined, { type: 'RECEIVE_THREADS', threads });
    expect(state).toEqual({ threads });
  });
});
