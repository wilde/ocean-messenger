import proxyquire from 'proxyquire';
import EventEmitter from 'events';
let electron;
let Actions;

describe('Actions', () => {
  beforeEach(() => {
    const mockIpcRenderer = new EventEmitter();
    electron = { ipcRenderer: mockIpcRenderer, '@noCallThru': true };
    Actions = proxyquire('../../renderer/actions', { electron });
  });
  describe('#fetchThreads', () => {
    it('should ask the main process for threads', () => {
      electron.ipcRenderer.send = jasmine.createSpy('send');
      Actions.fetchThreads();
      expect(electron.ipcRenderer.send).toHaveBeenCalledWith('slack', { type: 'FETCH_THREADS' });
    });
  });
});
