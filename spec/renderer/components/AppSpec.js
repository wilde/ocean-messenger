import React from 'react';
import { render } from 'enzyme';
import proxyquire from 'proxyquire';

let App;

describe('<App />', () => {
  beforeEach(() => {
    App = proxyquire('../../../renderer/components/App', { '../actions': { } }).default;
  });

  it('renders a list of threads', () => {
    class MockStore {
      getState() {
        return { threads: [{ id: 'thread1' }] };
      }
      subscribe() {}
      dispatch() {}
    }
    const wrapper = render(<App store={new MockStore()} />);
    expect(wrapper.find('ul').length).toEqual(1);
    expect(wrapper.find('li').length).toEqual(1);
  });
});
