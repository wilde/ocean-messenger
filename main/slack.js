import _ from 'lodash';
import rp from 'request-promise';
import promisify from 'es6-promisify';
import sqlite3 from 'sqlite3';
import { client as WebSocketClient } from 'websocket';
import url from 'url';
import querystring from 'querystring';
import { ipcMain } from 'electron';

sqlite3.verbose();

export default class Slack {
  constructor(filename) {
    this.db = new sqlite3.Database(filename);
    this.all = promisify(this.db.all, this.db);
    this.ipcListener = this.onRendererRequest.bind(this);
    ipcMain.on('slack', this.ipcListener);
  }
  upgradeSchema() {
    return new Promise((resolve) => {
      this.db.serialize(() => {
        this.db
          .run('PRAGMA foreign_keys = ON')
          .run(
            `CREATE TABLE IF NOT EXISTS config (
              id          NUMERIC   PRIMARY KEY,
              token       TEXT
            )`
          )
          .get('SELECT * FROM config WHERE id = 0', (error, config) => {
            if (config) {
              this.token = config.token;
            }
          })
          .run(
            `CREATE TABLE IF NOT EXISTS users (
              id          TEXT    PRIMARY KEY,
              team_id     TEXT,
              name        TEXT    NOT NULL,
              color       INTEGER NOT NULL,
              real_name   TEXT    NOT NULL,
              email       TEXT,
              image_url   TEXT
            )`
          )
          .run(
            `CREATE TABLE IF NOT EXISTS threads (
              id          TEXT      PRIMARY KEY,
              name        TEXT,
              created     DATETIME  NOT NULL,
              creator     TEXT      NOT NULL
            )`
          )
          .run(
            `CREATE TABLE IF NOT EXISTS thread_users (
              thread_id   TEXT  NOT NULL REFERENCES threads(id),
              user_id     TEXT  NOT NULL
            )`
          )
          .run(
            `CREATE UNIQUE INDEX IF NOT EXISTS idx_thread_users_on_ids
              ON thread_users (thread_id, user_id)`
          )
          .run(
            `CREATE TABLE IF NOT EXISTS messages (
              timestamp   NUMERIC   NOT NULL,
              thread_id   TEXT      NOT NULL REFERENCES threads(id),
              user_id     TEXT      NOT NULL,
              text        TEXT      NOT NULL,
              type        TEXT
            )`
          )
          .run(
            `CREATE UNIQUE INDEX IF NOT EXISTS idx_messages_on_thread_ts
              ON messages (thread_id, timestamp)`, resolve
          );
      });
    });
  }
  rowData(row) {
    return _.mapKeys(row, (value, key) => `$${key}`);
  }
  async upsertRow(tableName, idColumns, row) {
    const rowData = this.rowData(row);
    const updateStatement = await this.updateRow(tableName, idColumns, row, rowData);
    if (updateStatement.changes === 0) {
      await this.insertRow(tableName, row, rowData);
    }
  }
  insertRow(tableName, row, rowData) {
    return new Promise((resolve, reject) => {
      const columns = _.keys(row).join(', ');
      const values = _.keys(row).map((key) => `$${key}`).join(', ');
      const insertSql =
        `INSERT INTO ${tableName} (${columns}) VALUES (${values})`;
      this.db.run(insertSql, rowData, function insertCallback(error) {
        if (error) {
          reject(error);
        } else {
          resolve(this);
        }
      });
    });
  }
  updateRow(tableName, idColumns, row, rowData) {
    return new Promise((resolve, reject) => {
      const columnFunc = (key) => `${key} = $${key}`;
      const setExpr = _.keys(row).map(columnFunc).join(', ');
      const whereExpr = idColumns.map(columnFunc).join(' AND ');
      const updateSql =
        `UPDATE ${tableName} SET ${setExpr} WHERE ${whereExpr}`;
      this.db.run(updateSql, rowData, function updateCallback(error) {
        if (error) {
          reject(error);
        } else {
          resolve(this);
        }
      });
    });
  }
  async upsertUsers(users) {
    const upserts = users.map((user) =>
      this.upsertRow('users', ['id'], {
        id: user.id,
        team_id: user.team_id,
        name: user.name,
        color: user.color,
        real_name: user.real_name,
        email: user.email,
        image_url: user.profile.image_512,
      })
    );
    await Promise.all(upserts);
  }
  async upsertChannels(channels) {
    const channelUpserts = channels.map((channel) =>
      this.upsertRow('threads', ['id'], {
        id: channel.id,
        name: channel.name,
        created: channel.created,
        creator: channel.creator,
      })
    );
    await Promise.all(channelUpserts);

    const run = promisify(this.db.run, this.db);
    const memberUpserts = _.flatMap(channels, async (channel) => {
      await run('DELETE FROM thread_users WHERE thread_id = $thread_id', {
        $thread_id: channel.id,
      });
      const memberInserts = channel.members.map((userId) => {
        const memberRow = {
          thread_id: channel.id,
          user_id: userId,
        };
        return this.insertRow('thread_users', memberRow, this.rowData(memberRow));
      });
      await Promise.all(memberInserts);
    });
    await Promise.all(memberUpserts);
  }
  async getToken(authorizationCode) {
    const response = await rp({
      uri: 'https://slack.com/api/oauth.access',
      qs: {
        client_id: Slack.clientId,
        client_secret: Slack.clientSecret,
        code: authorizationCode,
      },
      json: true,
    });
    if (!response.ok) {
      throw response.error;
    }
    this.token = response.access_token;
    await this.upsertRow('config', ['id'], { id: 0, token: this.token });
  }
  async syncMessages(channel, latestTimestamp = 0) {
    const response = await rp({
      uri: 'https://slack.com/api/channels.history',
      qs: {
        token: this.token,
        channel,
        latest: latestTimestamp,
      },
      json: true,
    });
    if (!response.ok) {
      throw response.error;
    }
    const upserts = response.messages.
      filter((message) => message.type === 'message').
      map((message) =>
        this.upsertRow('messages', ['thread_id', 'timestamp'], {
          timestamp: message.ts,
          thread_id: channel,
          user_id: message.user,
          text: message.text,
          type: message.subtype,
        })
      );
    await Promise.all(upserts);
  }
  async listenForEvents() {
    const response = await rp({
      uri: 'https://slack.com/api/rtm.start',
      qs: { token: this.token, mpim_aware: true, simple_latest: true },
      json: true,
    });
    if (!response.ok) {
      throw response.error;
    }
    await Promise.all([
      this.connect(response.url),
      this.upsertUsers(response.users),
      this.upsertChannels(response.channels),
    ]);
  }
  connect(socketUrl) {
    return new Promise((resolve, reject) => {
      const websocket = new WebSocketClient();
      websocket.on('connect', (connection) => {
        this.connection = connection;
        this.connection.on('message', this.onWebsocketMessage.bind(this));
        resolve(connection);
      });
      websocket.on('connectFailed', reject);
      websocket.connect(socketUrl);
    });
  }
  onWebsocketMessage(message) {
    if (message.type !== 'utf8') {
      const error = new Error('Unexpected binary message');
      error.message = message;
      throw error;
    }
    const event = JSON.parse(message.utf8Data);
    switch (event.type) {
      case 'message':
        return this.upsertRow('messages', ['thread_id', 'timestamp'], {
          timestamp: event.ts,
          thread_id: event.channel,
          user_id: event.user,
          text: event.text,
          type: event.subtype,
        });
      case 'channel_created':
        return this.upsertChannels([event.channel]);
      case 'team_join':
      case 'user_change':
        return this.upsertUsers([event.user]);
      default:
        return Promise.resolve();
    }
  }
  async close() {
    this.connection.close();
    ipcMain.off('slack', this.ipcListener);
    this.ipcListener = null;

    await promisify(this.db.close, this.db)();
  }
  authenticate(browserWindow) {
    return new Promise((resolve) => {
      if (this.token) {
        resolve();
      } else {
        const oauthParams = querystring.stringify({
          client_id: Slack.clientId,
          scope: 'client',
        });
        browserWindow.webContents.once('will-navigate', async (event, redirectUri) => {
          const parsedUri = url.parse(redirectUri, true);
          await this.getToken(parsedUri.query.code);
          resolve();
        });
        browserWindow.loadURL(`https://slack.com/oauth/authorize?${oauthParams}`);
      }
    });
  }
  async onFetchThreads(sender) {
    const threads = await this.all('SELECT * FROM threads');
    sender.send('slack', { type: 'RECEIVE_THREADS', threads });
  }
  onRendererRequest(event, action) {
    switch (action.type) {
      case 'FETCH_THREADS':
        return this.onFetchThreads(event.sender);
      default:
        return Promise.resolve();
    }
  }
}

Slack.clientId = '28030039732.35273637059';
Slack.clientSecret = '3037dd79fcc35d58fd5ceb3acf24c0cc';
