import proxyquire from 'proxyquire';
proxyquire.noCallThru();
import EventEmitter from 'events';
import install from 'jasmine-es6/overrides/async';
install();

describe('Application', () => {
  describe('onAppReady', () => {
    let app;
    let mockWindow;

    class MockSlack {
      async upgradeSchema() { }
      async authenticate() { }
      async listenForEvents() { }
      async close() { }
    }
    function mockApplication() {
      return proxyquire('../../main/application', {
        electron: { app, BrowserWindow: () => mockWindow },
        './slack': MockSlack,
      }).default;
    }

    beforeEach(() => {
      app = new EventEmitter();
      mockWindow = new EventEmitter();
      mockWindow.loadURL = jasmine.createSpy('loadURL');
      mockWindow.webContents = new EventEmitter();
      mockWindow.webContents.openDevTools = () => {};
    });
    it('should make a new window', async () => {
      const Application = mockApplication();
      const mainApp = new Application(true);
      await mainApp.onAppReady();
      expect(mockWindow.loadURL).toHaveBeenCalled();
    });
    describe('when the main window is closed', () => {
      it('should close the slack store', async () => {
        const Application = mockApplication();
        const mainApp = new Application(true);
        await mainApp.onAppReady();
        spyOn(mainApp.slackStore, 'close').and.callThrough();

        mockWindow.emit('closed');
        expect(mainApp.slackStore.close).toHaveBeenCalled();
      });
    });
  });
});
